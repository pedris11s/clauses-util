Update Template set Tags='CustomerCode-DEFAULT,ConnectPoliticasPrivacidad', DocType='PPRIVACIDAD' where Slug='pconnectdefault';

Update Template set Tags='CustomerCode-BCI,ConnectTermsAndConditions', DocType='TÉRMINOS' where Slug='tconnectbci';
Update Template set Tags='CustomerCode-DEFAULT,ConnectTermsAndConditions', DocType='TÉRMINOS' where Slug='tconnectdefault';

select Slug, Tags, DocType from Template order by Created desc;

-- delete from Clause Where Id='51a38fd2-72ca-4590-b593-60abf33c195e';
-- select * from Clause order by Updated desc limit 1;
