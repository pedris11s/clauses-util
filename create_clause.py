import json
import os, sys
from python_graphql_client import GraphqlClient

def create_clause_gql(ownerId, ownerName, template, key, name, description, version, varss=None, data=None):
    # # dev
    # TOU_GQL_URL = "https://tou-gql-dev.gql.elegalbase.io"
    # TOU_GQL_KEY = "da2-n45pbrywknfibbhhbvnaxe4kwy"

    # # qa
    TOU_GQL_URL = 'https://tou-gql-qa.gql.elegalbase.io'
    TOU_GQL_KEY = 'da2-bzwzvklmkrd35ovmot5bqfak54'
    
    # # homo
    # TOU_GQL_URL = 'https://tou-gql-homo.gql.legalbase.io'
    # TOU_GQL_KEY = 'da2-yd24m4n2pjdpjf5olgqsxrwgum'
    
    # # prod
    # TOU_GQL_URL = 'https://tou-gql.gql.legalbase.io'
    # TOU_GQL_KEY = 'da2-327q3wygrjgqlinve3xmbndkxy'

    client = GraphqlClient(endpoint=TOU_GQL_URL, headers={'x-api-key': TOU_GQL_KEY})
    query = """
        mutation CreateClause($input: CreateClauseInput!){
            CreateClause(input: $input){
                Code
                Message
            }
        }
    """

    variables = {
       "input": {
            "OwnerId": ownerId,
            "OwnerName": ownerName,
            "Template": template,
            "ClauseKey": key,
            "ClauseName": name,
            "ClauseDescription": description,
            "Version": version,
            "Variables": json.dumps(varss),
            "Data": json.dumps(data)
       }
    }

    response = client.execute(query, variables)
    print("{} {}".format(name ,json.dumps(response)))

def loadTemplate(name):
    file_path = f"{name}.html"
    f = open(file_path, "r", encoding="utf-8")
    return f.read()


def loadData():
    file_path = f"clauses-data.json"
    f = open(file_path, "r", encoding="utf-8")
    return f.read()

def create_clause(clause_name, key, name):
    # template_name = 'pp_connect_default'
    template_name = clause_name
    # clauseName = f"revision8_{template_name}"
    ownerId = "datamart"
    ownerName = "Datamart Spa"
    html = loadTemplate(template_name)
    description = ""
    version = 1

    data = loadData()
    data = json.loads(data)
    

    create_clause_gql(ownerId, ownerName, html, key, name, description, version, data[template_name]['variables'], data[template_name]['data'])



if __name__ == '__main__':
    
    # dir_path = os.path.dirname(os.path.realpath(__file__))
    # parent_dir_path = os.path.abspath(os.path.join(dir_path, os.pardir))
    # sys.path.insert(0, parent_dir_path)

    create_clause("tou_connect_persona", "TermsConnect", "Términos Connect Persona Natural")
    
    # create_clause("tou_connect_default", "TermsConnect", "Términos Connect Empresa")
    
    # create_clause("tou_connect_bci", "TermsConnect", "Términos Connect BCI")
    
    # create_clause("pp_connect_default", "PPConnect", "Política Privacidad Connect Default")
